import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from ImageProcessLib import *
from argparse import ArgumentParser

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-img', help='image path', type=str)
    parser.add_argument('--channel', '-c', help='image channel', type=int)
    parser.add_argument('--mode', '-m', help='image process mode', type=str)
    parser.add_argument('--grammar', '-r', help='grammar value', type=float)
    parser.add_argument('--filter_size', help='filter size', type=tuple, default=['3', '3'])
    parser.add_argument('--mask', help='mask matrix', nargs='+', type=int, default=[0, -1, 0, -1, 4, -1, 0, -1, 0])
    parser.add_argument('--save', '-s', help='save image name', type=str)
    args = parser.parse_args()

    if args.channel == 3:
        img = cv2.imread(args.img)
        b, g, r = cv2.split(img)
    elif args.channel == 1:
        img = cv2.imread(args.img, 0)
    else:
        pass

    filter_size = []
    for val in args.filter_size:
        if val.isdigit():
            filter_size.append(int(val))

    if args.mode == 'gray':
        transformImg = rgb2gray(r, g, b).convert()
    elif args.mode == 'correction':
        transformImg = originalCorrection(img).convert()
    elif args.mode == 'grammar_correction':
        transformImg = grammarCorrection(img, args.grammar).convert()
    elif args.mode == 'negative_film':
        transformImg = negativeFilm(img).convert()
    elif args.mode == 'salt_and_pepper':
        transformImg = saltAndPepperNoise(img).convert()
    elif args.mode == 'median_filter':
        transformImg = medianFilter(img, tuple(filter_size)).convert()
    elif args.mode == 'laplacian':
        transformImg = laplacian(img, np.asarray(args.mask), tuple(filter_size)).convert()
    elif args.mode == 'max_filter':
        transformImg = maxFilter(img, tuple(filter_size)).convert()
    elif args.mode == 'binarization':
        transformImg = binarization(img).convert()

    plt.imsave(args.save, transformImg, cmap='gray')