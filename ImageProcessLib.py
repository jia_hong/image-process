import inspect
import numpy as np
from inspect import signature

class Typed:
    def __init__(self, name, required_type):
        self.name = name
        self.required_type = required_type

    def __get__(self, instance, cls):
        if instance is None:
            return self
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if not isinstance(value, self.required_type):
            raise TypeError('Attribute {} expected {}'.format(self.name, self.required_type))
        instance.__dict__[self.name] = value

    def __delete__(self, instance):
        del instance.__dict__[self.name]

def typeassert(cls):
    params = signature(cls).parameters
    for name, param in params.items():
        if param.annotation != inspect._empty:
            setattr(cls, name, Typed(name, param.annotation))
    return cls

@typeassert
class rgb2gray:
    def __init__(self, r: np.ndarray, g: np.ndarray, b: np.ndarray):
        self.r = r
        self.g = g
        self.b = b

    def convert(self) -> np.ndarray:
        return np.round(self.r*0.299 + self.g*0.587 + self.b*0.114)

@typeassert
class originalCorrection:
    def __init__(self, img: np.ndarray):
        self.img = img

    def convert(self) -> np.ndarray:
        max_pixel = np.max(self.img)
        min_pixel = np.min(self.img)

        return np.round(((self.img - min_pixel) / (max_pixel - min_pixel)) * 255)

@typeassert
class grammarCorrection:
    def __init__(self, img: np.ndarray, grammar: float):
        self.img = img
        self.grammar = grammar

    def convert(self) -> np.ndarray:
        max_pixel = np.max(self.img)
        min_pixel = np.min(self.img)

        return np.round((((self.img - min_pixel) / (max_pixel - min_pixel)) ** self.grammar) * 255)

@typeassert
class negativeFilm:
    def __init__(self, img: np.ndarray):
        self.img = img

    def convert(self) -> np.ndarray:
        return 255 - self.img

@typeassert
class saltAndPepperNoise:
    def __init__(self, img: np.ndarray):
        self.img = img

    def convert(self) -> np.ndarray:
        (height, width) = self.img.shape
        color = [0, 255]
        threshold = 0.85

        for row in range(height):
            for col in range(width):
                prop = np.random.random()
                if prop > threshold:
                    self.img[row][col] = color[np.random.randint(2)]
                else:
                    pass

        return self.img

@typeassert
class medianFilter:
    def __init__(self, img: np.ndarray, filter_size: tuple):
        self.img = img
        (self.filter_w, self.filter_h) = filter_size
        assert self.filter_w == self.filter_h

    def convert(self) -> np.ndarray:
        (height, width) = self.img.shape
        n_img = self.img.copy()
        interval_h = int(self.filter_h/2)
        interval_w = int(self.filter_w/2)

        for row in range(interval_h, height - interval_h):
            for col in range(interval_w, width - interval_w):
                window = []
                for x in range(-interval_w, self.filter_w - interval_w):
                    for y in range(-interval_h, self.filter_h - interval_h):
                        window.append(self.img[row + x][col + y])
                window.sort()
                n_img[row][col] = window[int(self.filter_w * self.filter_w/2)]

        return n_img

@typeassert
class maxFilter:
    def __init__(self, img: np.ndarray, filter_size: tuple):
        self.img = img
        (self.filter_w, self.filter_h) = filter_size
        assert self.filter_w == self.filter_h

    def convert(self) -> np.ndarray:
        (height, width) = self.img.shape
        n_img = self.img.copy()
        interval_h = int(self.filter_h/2)
        interval_w = int(self.filter_w/2)

        for row in range(interval_h, height - interval_h):
            for col in range(interval_w, width - interval_w):
                window = []
                for x in range(-interval_w, self.filter_w - interval_w):
                    for y in range(-interval_h, self.filter_h - interval_h):
                        window.append(self.img[row + x][col + y])
                n_img[row][col] = np.max(window)

        return n_img

@typeassert
class laplacian:
    def __init__(self, img: np.ndarray, mask: np.ndarray, filter_size: tuple):
        self.img = img
        (self.filter_w, self.filter_h) = filter_size
        self.mask = mask

        assert self.filter_w == self.filter_h
        assert len(self.mask) == self.filter_w * self.filter_h

    def convert(self) -> np.ndarray:
        mask = self.mask
        (height, width) = self.img.shape
        n_img = self.img.copy()
        interval_h = int(self.filter_h/2)
        interval_w = int(self.filter_w/2)

        for row in range(interval_h, height - interval_h):
            for col in range(interval_w, width - interval_w):
                window = []
                for x in range(-interval_w, self.filter_w - interval_w):
                    for y in range(-interval_h, self.filter_h - interval_h):
                        window.append(self.img[row + x][col + y])
                n_img[row][col] = np.sum(window * mask) if np.sum(window * mask) > 0 else 0

        return n_img

@typeassert
class binarization:
    def __init__(self, img: np.ndarray):
        self.img = img

    def convert(self) -> np.ndarray:
        threshold = round(np.mean(self.img))
        (height, width) = self.img.shape

        for row in range(0, height):
            for col in range(0, width):
                self.img[row][col] = 0 if threshold > self.img[row][col] else 255

        return self.img