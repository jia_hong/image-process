python main.py -img IU.jpg -c 3 -m gray -s gray.jpg
python main.py -img gray.jpg -c 1 -m correction -s correction.jpg
python main.py -img gray.jpg -c 1 -m grammar_correction -s grammar_correction_0.5.jpg -r 0.5
python main.py -img gray.jpg -c 1 -m grammar_correction -s grammar_correction_1.5.jpg -r 1.5
python main.py -img gray.jpg -c 1 -m negative_film -s negative_film.jpg
python main.py -img grammar_correction_0.5.jpg -c 1 -m salt_and_pepper -s salt_and_pepper.jpg
python main.py -img salt_and_pepper.jpg -c 1 -m median_filter -s median_filter.jpg
python main.py -img correction.jpg -c 1 -m laplacian --mask -1 -1 -1 -1 8 -1 -1 -1 -1 -s laplacian.jpg
python main.py -img laplacian.jpg -c 1 -m max_filter -s max_filter.jpg
python main.py -img grammar_correction_1.5.jpg -c 1 -m binarization -s binarization.jpg